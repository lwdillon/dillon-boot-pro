package com.lw.dillon.module.infra.dal.mysql.db;

import com.lw.dillon.framework.mybatis.core.mapper.BaseMapperX;
import com.lw.dillon.module.infra.dal.dataobject.db.DataSourceConfigDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 数据源配置 Mapper
 *
 * @author liwen
 */
@Mapper
public interface DataSourceConfigMapper extends BaseMapperX<DataSourceConfigDO> {
}

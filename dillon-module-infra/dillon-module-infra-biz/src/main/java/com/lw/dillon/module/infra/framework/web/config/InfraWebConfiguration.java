package com.lw.dillon.module.infra.framework.web.config;

import com.lw.dillon.framework.swagger.config.DillonSwaggerAutoConfiguration;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * infra 模块的 web 组件的 Configuration
 *
 * @author liwen
 */
@Configuration(proxyBeanMethods = false)
public class InfraWebConfiguration {

    /**
     * infra 模块的 API 分组
     */
    @Bean
    public GroupedOpenApi infraGroupedOpenApi() {
        return DillonSwaggerAutoConfiguration.buildGroupedOpenApi("infra");
    }

}

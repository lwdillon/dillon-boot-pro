package com.lw.dillon.module.report.dal.mysql.goview;

import com.lw.dillon.framework.common.pojo.PageParam;
import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.framework.mybatis.core.mapper.BaseMapperX;
import com.lw.dillon.framework.mybatis.core.query.LambdaQueryWrapperX;
import com.lw.dillon.module.report.dal.dataobject.goview.GoViewProjectDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GoViewProjectMapper extends BaseMapperX<GoViewProjectDO> {

    default PageResult<GoViewProjectDO> selectPage(PageParam reqVO, Long userId) {
        return selectPage(reqVO, new LambdaQueryWrapperX<GoViewProjectDO>()
                .eq(GoViewProjectDO::getCreator, userId)
                .orderByDesc(GoViewProjectDO::getId));
    }

}

package com.lw.dillon.module.system.convert.logger;

import com.lw.dillon.framework.common.pojo.PageResult;
import com.lw.dillon.module.system.api.logger.dto.LoginLogCreateReqDTO;
import com.lw.dillon.module.system.controller.admin.logger.vo.loginlog.LoginLogExcelVO;
import com.lw.dillon.module.system.controller.admin.logger.vo.loginlog.LoginLogRespVO;
import com.lw.dillon.module.system.dal.dataobject.logger.LoginLogDO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface LoginLogConvert {

    LoginLogConvert INSTANCE = Mappers.getMapper(LoginLogConvert.class);

    PageResult<LoginLogRespVO> convertPage(PageResult<LoginLogDO> page);

    List<LoginLogExcelVO> convertList(List<LoginLogDO> list);

    LoginLogDO convert(LoginLogCreateReqDTO bean);

}

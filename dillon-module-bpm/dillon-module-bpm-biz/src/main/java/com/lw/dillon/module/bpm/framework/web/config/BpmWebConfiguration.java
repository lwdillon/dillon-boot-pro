package com.lw.dillon.module.bpm.framework.web.config;

import com.lw.dillon.framework.swagger.config.DillonSwaggerAutoConfiguration;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * bpm 模块的 web 组件的 Configuration
 *
 * @author liwen
 */
@Configuration(proxyBeanMethods = false)
public class BpmWebConfiguration {

    /**
     * bpm 模块的 API 分组
     */
    @Bean
    public GroupedOpenApi bpmGroupedOpenApi() {
        return DillonSwaggerAutoConfiguration.buildGroupedOpenApi("bpm");
    }

}

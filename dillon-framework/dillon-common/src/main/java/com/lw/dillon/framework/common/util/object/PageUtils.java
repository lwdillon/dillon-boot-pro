package com.lw.dillon.framework.common.util.object;

import com.lw.dillon.framework.common.pojo.PageParam;

/**
 * {@link com.lw.dillon.framework.common.pojo.PageParam} 工具类
 *
 * @author liwen
 */
public class PageUtils {

    public static int getStart(PageParam pageParam) {
        return (pageParam.getPageNo() - 1) * pageParam.getPageSize();
    }

}

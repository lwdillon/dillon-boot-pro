package com.lw.dillon.framework.common.core;

/**
 * 可生成 Int 数组的接口
 *
 * @author liwen
 */
public interface IntArrayValuable {

    /**
     * @return int 数组
     */
    int[] array();

}

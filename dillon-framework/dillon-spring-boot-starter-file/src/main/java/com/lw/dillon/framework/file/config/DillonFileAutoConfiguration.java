package com.lw.dillon.framework.file.config;

import com.lw.dillon.framework.file.core.client.FileClientFactory;
import com.lw.dillon.framework.file.core.client.FileClientFactoryImpl;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * 文件配置类
 *
 * @author liwen
 */
@AutoConfiguration
public class DillonFileAutoConfiguration {

    @Bean
    public FileClientFactory fileClientFactory() {
        return new FileClientFactoryImpl();
    }

}

package com.lw.dillon.framework.operatelog.config;

import com.lw.dillon.framework.operatelog.core.aop.OperateLogAspect;
import com.lw.dillon.framework.operatelog.core.service.OperateLogFrameworkService;
import com.lw.dillon.framework.operatelog.core.service.OperateLogFrameworkServiceImpl;
import com.lw.dillon.module.system.api.logger.OperateLogApi;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

@AutoConfiguration
public class DillonOperateLogAutoConfiguration {

    @Bean
    public OperateLogAspect operateLogAspect() {
        return new OperateLogAspect();
    }

    @Bean
    public OperateLogFrameworkService operateLogFrameworkService(OperateLogApi operateLogApi) {
        return new OperateLogFrameworkServiceImpl(operateLogApi);
    }

}

package com.lw.dillon.framework.pay.config;

import com.lw.dillon.framework.pay.core.client.PayClientFactory;
import com.lw.dillon.framework.pay.core.client.impl.PayClientFactoryImpl;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * 支付配置类
 *
 * @author liwen
 */
@AutoConfiguration
public class DillonPayAutoConfiguration {

    @Bean
    public PayClientFactory payClientFactory() {
        return new PayClientFactoryImpl();
    }

}

package com.lw.dillon.framework.test.core.ut;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 * 纯 Mockito 的单元测试
 *
 * @author liwen
 */
@ExtendWith(MockitoExtension.class)
public class BaseMockitoUnitTest {
}

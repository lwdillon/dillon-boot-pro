package com.lw.dillon.framework.sms.config;

import com.lw.dillon.framework.sms.core.client.SmsClientFactory;
import com.lw.dillon.framework.sms.core.client.impl.SmsClientFactoryImpl;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * 短信配置类
 *
 * @author liwen
 */
@AutoConfiguration
public class DillonSmsAutoConfiguration {

    @Bean
    public SmsClientFactory smsClientFactory() {
        return new SmsClientFactoryImpl();
    }

}
